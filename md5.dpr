program md5;
{$APPTYPE CONSOLE}
uses md5unit;

var h : MD5Digest;

begin
  if ParamCount() < 1 then begin
    writeln('Usage:');
    writeln('  md5.exe <filename>');
    exit;
  end
  else begin
    h:=md5file(ParamStr(1));
    writeln('MD5: ' + MD5Print(h));
  end;
end.
