program c4;

(*
  ������� �����ᮬ ᮢ������� ���� ��ப S1 � S2, ᮤ�ঠ�� ⮫쪮 �㪢� ��⨭᪮�� ��䠢��, ��뢠�� �᫮
  M Ic = C㬬� �� K �� 1 �� 26 (fk(1) * fk(2)/(m1 * m2)
  ��� m  ����� ��ப�
  fk(n) - �᫮ �宦����� �㪢� � ����஬ k � ��ப� Sn.

  ���ਬ��, ������ ᮢ������� ��ப "moloko"  "mama" �㤥�
  M Ic = 2/24 = 1/12

  �ॡ���� ������� �ணࠬ��, ����� ������ � ���������� ��� ��ப�, ᮤ�ঠ騥 �஬� �㪢 ⠪�� ����� �९������ � �஡���, � ������ �� ������� ������ ᮢ�������.
*)

var
  s1, s2 : string; // ������ ��ப�
  i      : word;   // ��६����
  m1, m2 : word;   // ����� ��ப� s1, s2
  f1, f2 : word;
  res    : real;
  c      : char;

begin
  write('Input string 1: '); readln(s1);
  write('Input string 2: '); readln(s2);
  m1 := length(s1); m2 := length(s2);
  res := 0;
  for c := 'A' to 'Z' do begin
    f1 := 0; f2 := 0;
    for i := 1 to m1 do
      if UpCase(s1[i]) = c then inc(f1);
    for i := 1 to m2 do
      if UpCase(s2[i]) = c then inc(f2);
    res := res + (f1*f2)/(m1*m2);
  end;
  write('Result: '); writeln(res:16:12);
  readln;
end.
